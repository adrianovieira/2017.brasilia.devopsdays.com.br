+++
categories = ["Development", "Operation"]
date = "2016-08-25T23:01:23-03:00"
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "DevOpsDays"
description = "Bringing Dev and Ops together"

+++

# Sobre o DevOpsDays Brasília

O DevOpsDays Brasília, associado e reconhecido pelo [DevOpsDays.org](http://www.devopsdays.org/), tem como objetivo reunir os interessados no assunto DevOps através da apresentação de palestras, painéis, minicursos e outras atividades.
Considerando o aumento do interesse no desenvolvimento de software e cultura Ágil no Brasil, acreditamos que falar sobre DevOps é uma necessidade. O DevOpsDays Brasília 2017 vem preencher essa lacuna, uma vez que esse assunto ainda não foi tratado de forma sistemática em um evento próprio, mesmo com a atual e crescente demanda no Brasil.
Pretendemos para esse evento convidar profissionais de empresas e organizações conhecidas no cenário de tecnologia da informação, para assim motivar a ampla participação, não somente de pessoas que já conhecem a cultura DevOps, mas também despertar o interesse daqueles que ainda não conhecem.

***Edições Anteriores***

[**2016**](/2016)

## Visão geral

Alguns chamam DevOps de um fenômeno de transformação da cultura de como administrar e desenvolver serviços, enquanto outros acreditam que seja apenas uma metodologia ágil de administrar sistemas. Uma coisa, entretanto, não há de se questionar: o termo DevOps foi cunhado em 2009, na cidade de Ghent, na Bélgica, em um evento chamado DevOpsDays.
Desde então esse evento tem acontecido em várias cidades ao redor do mundo, sempre tendo como objetivo principal disseminar essa nova metodologia/cultura sobre como os desenvolvedores e operadores de infraestrutura devem trabalhar juntos para oferecer um resultado mais rápido, seguro e eficiente.
DevOps é tido como um fenômeno por conta da rapidez e eficiência. É possível perceber que essa tendência trouxe ótimos resultados para as organizações que a praticam, sempre proporcionado uma vantagem competitiva, segurança e agilidade na entrega do seu negócio.
Empresas como Facebook, Rackspace, DigitalOcean, Google e muitas outras têm a cultura tão “enraizadas” que muitas vezes não se consegue perceber uma distinção entre os famosos papéis de “desenvolvedor” e “operador de infraestrutura”. A ideia é atuar como um único time e assim proporcionar uma maior eficiência na entrega e manutenção dos sistemas e serviços que compõe o negócio.
Por outro lado, a novidade do DevOps não se limita a novas práticas, pois com a mudança no paradigma da atividade, criou-se diversas ferramentas para auxiliar esse novo modelo de trabalho. Nesta linha, ferramentas de automação de infraestrutura, integração contínua, ambientes “self service” e muitas outras foram criadas e apresentadas, todas elas fortalecendo essa nova tendência.
