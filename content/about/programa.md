+++
categories = ["Development", "Operation"]
date = "2016-08-26T20:48:04-04:00"
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "O programa"
description = ""

+++

Teremos três formas de sessões:

1. Palestra de 25 minutos
1. Debate ou painel de 25 minutos
1. Mini-Palestra de 12 minutos

Durante o evento também teremos espaços para:

- **Ignições**: serão apresentadas durante as sessões *ignite*^[***ignite***: <https://www.devopsdays.org/ignite-talks-format/>]. Serão 4 sessões com até  7 minutos cada e com slides trocando automaticamente. Estes ignites acontecerão no auditório principal.
- **Sessões de Espaço Aberto**: As sessões iniciadas nos *ignites* serão levadas para os espaços abertos (*open spaces*^[***open spaces***: <https://www.devopsdays.org/pages/open-space-format>]) onde a discussão irá se aprofundar naqueles temas.
- **Roda Viva**: Oportunidades de debates de ampla participação do plublico presente como debatedor presente no centro da Roda (Open Fishbowl^[***Fishbowl***: <https://en.wikipedia.org/wiki/Fishbowl_(conversation)>])


<div class = "row">
  <div class = "col-md-4">
    <h3>Temas das trilhas</h3>
  </div>
</div>
<div class = "row">
  <div class = "col-md-2">
  </div>
  <div class = "col-md-8">
    <b>CAMS</b>:
      <ul>
        <li class="text-primary"><b>[C]</b>ulture (Cultura)</li>
        <li class="text-success"><b>[A]</b>utomation (Automação)</li>
        <li class="text-info"><b>[M]</b>etrics (métricas, monitoramento, gestão)</li>
        <li class="text-warning"><b>[S]</b>haring (compartilhamento)</li>
      </ul>
  </div>
</div>

<div class = "row">
  <div class = "col-md-12 text-center">
    <h2>A programação</h2>
  </div>
</div>

<div class = "row programacao">
  <div class = "col-md-6">
    <div class = "row">
      <div class = "col-md-12">
        <h3>1&ordm; Dia: 07 de novembro</h3>
        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-xs-12 col-sm-6 col-md-8 col-md-4 col-md-offset-0">
            <time>08:00-12:00</time>
            <room></room>
          </div>
          <div class = "col-md-8 box">
            Credenciamento
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>08:45-09:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            Abertura 1&ordm; dia
            <br />(key note speaker)
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-2 col-md-offset-0">
            <time>09:30-10:00</time>
          </div>
          <div class = "col-md-2">
            <!-- room -->
          </div>
          <div class = "col-md-8 box">
            Coffee-break
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>10:00-10:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 1</i> <b class="text text-primary">[C]</b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>10:30-11:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 2</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>11:00-11:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 3</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>11:30-12:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 4</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>12:00-13:30</time>
            <room>livre</room>
          </div>
          <div class = "col-md-8 box">
            <i>Almoço</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>13:00-14:00</time>
            <room><i>showcase room</i></room>
          </div>
          <div class = "col-md-8 box">
            <i>Demonstrações</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>14:00-14:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 5</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>14:30-15:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 6</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>15:00-15:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 7</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>15:30-16:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 8</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>16:00-16:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            Coffee-break
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>16:30-17:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 9</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>17:00-17:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 10</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>17:30-18:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 11</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

      </div>
    </div>
  </div><!-- end day 1 -->

  <div class = "col-md-6">
    <div class = "row">
      <div class = "col-md-12">
        <h3>2&ordm; Dia: 08 de novembro</h3>
        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-xs-12 col-sm-6 col-md-8 col-md-4 col-md-offset-0">
            <time>08:00-12:00</time>
            <room></room>
          </div>
          <div class = "col-md-8 box">
            Credenciamento
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>08:45-09:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            Abertura 2&ordm; dia
            <br />(key note speaker)
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-2 col-md-offset-0">
            <time>09:30-10:00</time>
          </div>
          <div class = "col-md-2">
            <!-- room -->
          </div>
          <div class = "col-md-8 box">
            Coffee-break
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>10:00-10:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 12</i> <b class="text text-primary">[C]</b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>10:30-11:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 13</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>11:00-11:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 14</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>11:30-12:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 15</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>12:00-13:30</time>
            <room>livre</room>
          </div>
          <div class = "col-md-8 box">
            <i>Almoço</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>13:00-14:00</time>
            <room><i>showcase room</i></room>
          </div>
          <div class = "col-md-8 box">
            <i>Demonstrações</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>14:00-14:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 16</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>14:30-15:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 17</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>15:00-15:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 18</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>15:30-16:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 19</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>16:00-16:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            Coffee-break
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>16:30-17:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 20</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>17:00-17:30</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 21</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

        <!-- this div is repeated for each timeslot -->
        <div class = "row">
          <div class = "col-md-4 col-md-offset-0">
            <time>17:30-18:00</time>
            <room>Auditório</room>
          </div>
          <div class = "col-md-8 box">
            <i>Slot 22</i> <b class="text text-primary"></b>: <br />
          </div>
        </div> <!-- end timeslot div -->

      </div>
    </div>
  </div><!-- end day 2 -->

  <!-- confraternização DevOpsDays -->

  <div class="ending">
  <time>19:00...</time> <br />
  <room>TBD</room>

  <h3>Confraternização DevOpsDays Brasília</h3>
  com amigos novos e de longa data <br><br>
  <i class="fa fa-heart fa-2x"> </i> <i class="fa fa-smile-o fa-2x"> </i>

  </div>



</div>

<small>Mudanças na grade podem ocorrer decorrentes de circunstâncias fora do controle desta organização. Em especial
quando for algo relacionado a agenda de trabalho de palestrantes ou questões de ordem pessoal dos mesmos. Nestes casos a organização vai remanejar o slot inserindo outro conteúdo no mesmo horário.</small>
