+++
categories = ["Development", "Operation"]
date = "2016-08-25T23:01:23-03:00"
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing", "Location"]
title = "Local do evento"
description = ""

+++

# Local

### Centro de Convenções do Grand Mercure Hotel

**Endereço**:  

- *Centro de Convenções*  
  GRAND MERCURE Brasilia  
  Eixo Monumental, SHN quadra 5, bloco G,  
  Asa Norte, Brasília/DF

O *Centro de Convenções* está localizado na Asa Norte, *Plano Piloto* de Brasília/DF tendo próximo a ele:

- Hotéis (no Setor Hoteleiro Norte/SHN)
- Restaurantes e shoppings (no Setor Comercial Norte/SCN)
- Pontos de interesse (como o Parque da Cidade, Torre de TV, Feira de Artesanato/*Feira da Torre*, Esplanada dos Ministérios, Praça dos Três Poderes etc)
- Hospitais (como o Setor Médico Hospitalar Norte/SMHN)

<hr />
Mostramos abaixo um mapa reduzido com refência à localização:

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="border: 1px solid black"
  src="http://www.openstreetmap.org/export/embed.html?bbox=-47.89401%2C-15.78566%2C-47.88559%2C-15.79025&amp;layer=mapnik&amp;marker=-15.78810%2C-47.89033">
</iframe>
<br/>
<small>
  <a class="btn btn-link" target="blank" href="http://www.openstreetmap.org/?mlat=-15.78821&amp;mlon=-47.89037#map=18/-15.78829/-47.88985">
    Ver Mapa Ampliado
  </a>
</small>
