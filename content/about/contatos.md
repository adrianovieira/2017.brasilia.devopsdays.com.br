+++
categories = ["Development", "Operation"]
date = "2016-08-23T11:30:05-03:00"
description = ""
draft = false
image = "/img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Metrics", "Sharing"]
title = "contatos"
email_organizers = "organizers-brasilia-2017@devopsdays.org"

+++

## Equipe Local

<div class="row">
  <div class="col-xl-3 col-lg-4 col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>Guto Carvalho</h3>
        <h4>Organizador</h4>
      </div>
      <img style="max-width: 185px; max-height: 185px"
           src="/img/gutocarvalho.jpg"
           alt="Guto Carvalho">
      <div class="card-block">
        <p class="card-text"></p>
        <a href="http://twitter.com/gutocarvalho" class="card-link"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>&nbsp;
        <a href="http://gutocarvalho.net"><i class="fa fa-home fa-2x" aria-hidden="true"></i></a>&nbsp;
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-4 col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>Taciano Tres</h3>
        <h4>Organizador</h4>
      </div>
      <img style="max-width: 185px; max-height: 185px"
           src="/img/tacianotres.jpg"
           alt="Taciano Tres">
      <div class="card-block">
        <p class="card-text"></p>
        <a href="http://twitter.com/tacianot" class="card-link"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>&nbsp;
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-4 col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>Lauro Silveira</h3>
        <h4>Organizador</h4>
      </div>
      <img style="max-width: 185px; max-height: 185px"
           src="/img/laurosilveira.jpg"
           alt="Lauro Silveira">
      <div class="card-block">
        <p class="card-text"></p>
        <a href="http://twitter.com/laurosn" class="card-link"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>&nbsp;
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-4 col-md-6">
    <div class="card">
      <div class="card-header">
        <h3>Adriano Vieira</h3>
        <h4>Organizador</h4>
      </div>
      <img style="max-width: 185px; max-height: 185px"
           src="/img/adrianovieira.jpg"
           alt="Adriano Vieira">
      <div class="card-block">
        <p class="card-text"></p>
        <a href="http://twitter.com/adriano_vieira" class="card-link"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>&nbsp;
        <a href="https://www.facebook.com/adriano.svieira"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></a>&nbsp;
        <a href="https://www.linkedin.com/in/adriano-vieira-a8779811"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a>&nbsp;
      </div>
    </div>
  </div>
</div>
<br><br>
Sigam-nos lá no *Twitter* [`https://twitter.com/devopsdaysbsb`](https://twitter.com/devopsdaysbsb) e fiquemos antenados aos acontecimentos.

## Time de Divulgação e Captação

Agradecemos os esforços de:

Aline Hubner, Dirceu Silva, Eustáquio Guimarães, Diego Aguilera, Rogério Fernandes Pereira, Rafael Gomes

Caso você queira entrar em contato conosco:

- pelo email: [<i class="fa fa-envelope-o"></i>`organizers-brasilia-2017@devopsdays.org`](mailto: organizers-brasilia-2017@devopsdays.org)
- via chat (*Telegram*): [<i class="fa fa-question-circle-o"></i>`https://t.me/DevOpsDaysBrasilia`](https://t.me/DevOpsDaysBrasilia)
