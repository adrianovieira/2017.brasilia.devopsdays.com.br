+++
title = "Participe do Slack especial do DevOpsDays Brasíia!"
description = "Conecte-se, participe das discussões e conheça outros profissionais!"
draft = false
tags = [
  "devops",
  "comunidade",
  "slack",
  "discussao",
  "debate",
  "desenvolvimento",
  "networking",
]
categories = [
  "Announcement",
  "News",
]
image = "img/home-bg.jpg"
date = "2017-07-24T09:15:56-03:00"

author = 'Julia Branco'

+++

O DevOpsDays é um evento criado com o objetivo de compartilhar conhecimento sobre DevOps no Brasil e unir os profissionais que têm interesse na metodologia. Por isso, para a edição 2017, lançamos o nosso canal do Slack - uma oportunidade incrível para fazer networking, discutir novidades do mercado de automação e criar conexões.

O acesso é gratuito e pode ser feito pelo link ou *QRCode* abaixo:

[Participe do Slack ![QR Code](/img/slack-qrchart.png)](https://join.slack.com/t/devopsdaysbrasilia/shared_invite/MjE2MjY0NDc4MDk5LTE1MDA3MzQzNDEtNTEyMGQ0MmNjNA)

Para fazer parte da nossa comunidade do Slack, basta clicar no link acima e solicitar o seu acesso. Ele será concedido pela nossa equipe e você poderá criar o seu perfil na plataforma para se juntar ao time de participantes :)

Ressaltamos que a comunidade do Slack seguirá as regras e os valores definidos pelo Código de Conduta do DevOpsDays. Para saber mais sobre o assunto, [clique aqui](/about/conduta/).

Vem conversar e compartilhar ideias com a gente!
