+++
categories = ["News"]
date = "2017-04-25T11:19:21-03:00"
description = "Telegram - o evento em tempo real num chat"
draft = false
image = "img/home-bg.jpg"
tags = ["DevOps", "Culture", "Automation", "Lean", "Chat", "online", "Contact"]
title = "Chat via Telegram"

+++

# Lean Contact

Pois é!!!

Abrimos mais um canal de contato conosco...

Agora também estamos no *Telegram* e vocês poderão usar mais esta forma de conversar com os participantes do evento e sobre o evento.

- via chat (*Telegram*): [<i class="fa fa-question-circle-o"></i>`https://t.me/DevOpsDaysBrasilia`](https://t.me/DevOpsDaysBrasilia)

Nós da organização do evento estaremos lá para ajudar e orientar no que for preciso e estiver ao nosso alcance de fazer.

**Atenção**, não esqueça de ler e praticar o [***código de conduta***](/about/conduta).
i
