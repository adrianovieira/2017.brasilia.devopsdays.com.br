+++
categories = ["Announcement", "News"]
date = "2017-07-25T22:54:45-03:00"
description = "As vagas são limitadas - clique aqui e inscreva-se!"
draft = false
image = "img/home-bg.jpg"
tags = ["ingressos", "inscrição", "devopsdays", "evento", "vagas", "brasilia", "DevOps"]
title = "Faça a sua inscrição no DevOpsDays Brasília 2017"

+++

As inscrições para o DevOpsDays já estão abertas! Em novembro, você poderá participar do maior evento sobre DevOps do Centro-Oeste e assistir a dois dias de palestras e debates com especialistas nacionais e internacionais, no Centro de Convenções do Hotel Grand Mercure.

Faça já a sua inscrição enquanto ainda temos vagas disponíveis:

[<button type="button" class="btn btn-primary navbar-btn">Inscreva-se!</button>](/about/registration)

Essa é a sua oportunidade para conversar com profissionais de DevOps e Infraestrutura. Além disso, serão discutidas as principais novidades e tendências do setor para 2018. Para o evento, são esperadas mais de 300 pessoas e a participação de palestrantes de empresas que são referências em automação de TI no Brasil e no exterior.

O principal objetivo do DevOpsDays é estimular o debate sobre o desenvolvimento ágil no Brasil, além do compartilhamento de boas práticas e melhoria da qualificação dos profissionais de TI. Por isso, falar sobre DevOps é uma necessidade e a cada dia mais importante para preencher essa lacuna de conhecimentos que é sentida no mercado de automação brasileiro. Como uma iniciativa inédita no país, o DevOpsDays está atraindo os olhares das grandes empresas governamentais e privadas e dos profissionais que lideram ou desejam fazer parte de equipes de alta performance.

No ano passado, tivemos a participação de cerca de 116 empresas, representadas entre os mais de 280 participantes. Este ano, o evento terá mais um dia para a apresentação de palestras e conteúdos por parte dos palestrantes selecionados.

Caso você queira palestrar no evento, a chamada de trabalhos está aberta... então [clique aqui](/about/cfp) para enviar a sua proposta.

Se você deseja apresentar a sua marca para o nosso público, formado por gestores e profissionais de TI de alto nível, [clique aqui](/about/sponsor) para conhecer mais sobre as cotas de patrocínio.
