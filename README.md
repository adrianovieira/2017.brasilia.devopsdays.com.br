[![build status](https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/badges/master/build.svg)](https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/commits/master)

# DevOpsDays Brasília

## Notas de reuniões

[Página inicial para algumas notas das reuniões](notas_de_reunioes)

## Hotsite

O *hotsite* está disponível nos seguintes endereços (ambientes):
- homologação: <http://bsb.devopsdays.com.br:1313>
- produção: <http://devopsdays.bsb.br>

O ambiente para o *hotsite* é implementado sob as tecnologias:

- Hugo-0.17
- Rancher-1.1.3 (ou superior)
- Docker-1.12 (ou superior, mas que seja suportada pelo Rancher)

Todo o processo de produção de conteúdo e publicação nos ambientes é automatizado e realizado via *`gitlab-CI`*.

## Estado do build das *branches*

- ***master*** [![build status](https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/badges/master/build.svg)](https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/commits/master)

- ***branches/tags***, exemplo:
  - `branch 2017.1.1-beta2`: [![build status](https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/badges/2017.1.1-beta2/build.svg)](https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/tree/2017.1.1-beta2) **já teve MR**
  - `tag 2017.1.1`: [![build status](https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/badges/2017.1.1/build.svg)](https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/tree/2017.1.1)

## Contribuições com conteúdo

Realizar publicações no hotsite [brasilia.devopsdays.com.br](http://brasilia.devopsdays.com.br)

O site é feito e mantido em [Hugo](http://gohugo.io).

- adicionar palestrantes:
   - dados do palestrante (ex: `archetypes/speakers.yml`) devem ser criados no diretório `data/speakers`
   - dados da palestra (ex: `archetypes/program.md`) devem ser criados no diretório `content/program` com o mesmo nome dado ao ***yml*** do palestrante. Caso haja *co-palestrante* é nesse arquivo `.md` que deverão ser atualizadas as variáveis com seus respectivos dados.

- atualizar programa:
   - atualizar arquivo `content/about/programa.md`

- **adicionar post/notícias**:
   - adicionar arquivo `.md` no diretório `content/post`. É possível gerar notícias para aparecerem em data futura.

## processo de publicação

Todo o processo de criação/atualização de conteúdo pode ser feito via navegador, diretamente no projeto aqui no *`gitlab.com`*.

- passos para publicar em homologação
  1. criar nova `branch` a partir da `master` segundo o issue a ser resolvido
  1. para publicar em **homologacao** gerar branch com nome contendo ***`-betaNNN`*** (ex: `2017.1.1-beta1`)

- passos para publicar em produção
  1. criar nova release (`tag` )  partir da **`master`**. Use formato `M.m.r` (ex: `2017.1.1`), seguindo o padrão https://gitlab.com/devops-df/2017.brasilia.devopsdays.com.br/tags

**Atenção**:

Somente gere ***`tags`***, principalmente, após ter certeza que o conteúdo a ser publicado está realmente pronto!

## Site recovery

Para o caso de haver necessidade de recriação do *Hotsite* será necessário:

1. instalar Docker ( >=1.12.6 e <= 17.03.x-ce)
1. implementar Rancher (simples e não HA)
  1. Rancher server: `sudo docker run -d --restart=unless-stopped -p 8080:8080 rancher/server:v1.6.2`
  1. Rancher host: adicione pelo menos um  
     seguindo as orientações para adição de hosts ao enviroment (*custom* ou em algum provedor)
1. Configurar Rancher para o *hotsite*
  1. *UI*: configure o ***controle de acesso*** ao ambiente Rancher (`admin->acces`)
  1. *UI*: configure o ***catálogo de aplicações*** (`admin->settings`, `Catalog`, `Add catalog`)  
     Adicione o catálogo:  
     nome: **DevOps-DF**
      url: **`https://gitlab.com/devops-df/rancher-catalog`**
  1. *UI*: instale o hotsite via ***catálogo de aplicações*** (`Catalog`) respondendo ao questionário da versão de catálogo a ser implementado.  
    ![rancher-catalog-sitedeploy](doc/img/rancher-catalog-sitedeploy.png)
    ![rancher-catalog-sitedeploy-setup](doc/img/rancher-catalog-sitedeploy-setup.png)


---

***Keep CALMS and have fun***
