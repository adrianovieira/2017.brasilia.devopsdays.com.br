+++
tags = [
    "DevOps",
    "Culture",
    "Automation",
    "Lean",
    "Metrics",
    "Sharing",
]
categories = [
    "Development",
    "Operation",
    "Announcement",
    "News"
]
image = "img/home-bg.jpg" #optional image - "img/home-bg.jpg" is the default
description = ""
draft = true
+++
