+++
tags = [
    "DevOps",
    "Culture",
    "Automation",
    "Lean",
    "Metrics",
    "Sharing",
]
categories = [
    "Talk",
    "Palestra",
]
description = "Nome do Palestrante"
draft = true

copalestrante_name = ""
copalestrante_bio = ""
copalestrante_website = ""
copalestrante_twitter = ""
copalestrante_photoUrl = ""
+++

<!-- {{ .talk.title }} insira abaixo a título da palestra -->
# <span class="text text-success">Título da palestra</span>

<!-- {{.talk.description}} insira abaixo a descrição da palestra -->
Descrição da palestra

<!-- {{.talk.track}}
Temas das trilhas:
  <span class="text-primary">Culture (Cultura)</span>
  <span class="text-success">Automation (Automação)</span>
  <span class="text-info">Metrics (métricas, monitoramento, gestão)</span>
  <span class="text-warning">Sharing (compartilhamento)</span>
-->
- **Trilha**: <span class="text text-TRILHACOLOR">TEMA DA TRILHA</span>

<!-- {{.talk.level}} Iniciante / Intermediário / Avançado -->
- **Público alvo**:
